# Project no. 16

## Task

This is assignment no. 16 and it represents continuation of my last project. The main goal is to add some jQuery code, especially animations.

## To do

- add a form and animate its elements;
- connect to database;
- add a search option;