let App = (function (axios, Mustache) {	
	// define title of the project
	const mainTitle = 'Project no. 16';

	let genres = [];
	let movies = [];	

	// dom element with $
	let $genres = document.getElementById('genres');
	let $movies = document.getElementById('movies');
	// added
	let $infoWindow = document.getElementById('window');

	let genresTpl = document.getElementById('genres-template').innerHTML;
	let moviesTemplate = document.getElementById('movies-template').innerHTML;	
	let movieDetails = document.getElementById('movie-details').innerHTML;	

	return {
		start: function () {
			let self = this;

			this.attachEvents();
			this.fetch()
			.then(function (response) {
				let data = response.data;
				genres = data.genres;
				movies = data.movies;
				self.createLinks();
			});
			// title animation
			$(function() {
				// select title element
				let $mainTitle = $('#maintitle');
				// split title to letters
				let $mainTitleLetters = mainTitle.split("");
				console.log('title letters', $mainTitleLetters);
				// loop through letters
				for (let i = 0; i < $mainTitleLetters.length; i++) {
					// each letter surrounded with a span
					let newElement = $("<span/>").text($mainTitleLetters[i]).css({
						"opacity" : "0",
						"font-size" : "130px",
						"color" : "maroon"
					});
					console.log($mainTitleLetters[i]);
					// add letters
					newElement.appendTo($mainTitle);
					// set delay
					newElement.delay(i * 150);
					// return oppacity back to 1
					newElement.animate({
						"opacity" : "1",
						"font-size" : "160px"
					}, 1100);
					newElement.delay(i * 150);
					// return oppacity back to 1
					newElement.animate({
						"opacity" : "0.8",
						"font-size" : "150px"
					}, 1100);
				  };
			  });
		},
		fetch: function (cb) {
			return axios.get('data/db.json');
		},
		createLinks: function () {
			$genres.innerHTML = Mustache.render(genresTpl, {
				genres: genres
			});
		},
		updateMovies: function () {
			//console.log(movies[1].genres[1]);
			// TODO: compile movies template;
			console.log(window.location.hash);
			console.log(window.location.hash.slice(2));
			console.log(movies.length);
			console.log(movies[1].title);

			let myGenre = window.location.hash.slice(2);
			let listOfMovies = [];
			for (let i=0; i<movies.length; i++) {
				console.log(movies[i].title);
				if (movies[i].genres.includes(myGenre)) {
					listOfMovies.push(movies[i])
				}
			}
			
				$movies.innerHTML = Mustache.render(moviesTemplate, {
					movies:listOfMovies
				});
				// fade in effect
				let time = 1000;
				$('.movie').each(function (index, value) {
					$(this).fadeIn(time);
					time += 300;
					// this is for testing purposes
					console.log('div' + index + ':' + $(this).attr('id'));
					//console.log('this', this);
				  });
		},
		getMovieById: function(idClick) {
			let aMovie = movies[idClick];
			console.log(aMovie);

			$infoWindow.innerHTML = Mustache.render(movieDetails, {
				movies:aMovie
			});
			/*
			let parameter = '<input class="btn" id="close" type="button" value="close">';
			parameter += '<p class="infopar"><span class="info">Database ID: </span>' + aMovie.id + '<br>';
			parameter += '<span class="info">Title: </span>' + aMovie.title + '<br>';
			parameter += '<span class="info">Year: </span>' + aMovie.year + '<br>';
			parameter += '<span class="info">Runtime: </span>' + aMovie.runtime + '<br>';
			parameter += '<span class="info">Genre: </span>' + aMovie.genres.join(", ") + '<br>';
			parameter += '<span class="info">Director: </span>' + aMovie.director + '<br>';
			parameter += '<span class="info">Actors: </span>' + aMovie.actors + '<br>';
			parameter += '<span class="info">Plot: </span>' + aMovie.plot + '<br>';
			parameter += '<img class="image2" src="' + aMovie.posterUrl + '"alt="poster"></p>';
			$infoWindow.innerHTML = parameter;
			*/
		},
		// add class 'showme' and display info window
		infoMovie: function(idClick) {
			console.log($infoWindow);
			this.getMovieById(idClick);
			//$infoWindow.classList.add('showme');
			$('#window').slideDown(1500);
		},
		// close window with movie info
		closeInfo: function() {
			//$infoWindow.classList.remove('showme');
			$('#window').slideUp(1000);
		},
		// check click and decide what to do with it
		checkClick: function(e) {
			console.log(e.target.id);
			if (e.target.id.slice(0, 2) === 'no' || e.target.id.slice(0, 2) === 'nu') {
				// extract string, convert to number and reduce 1 (array starts at 0)
				let idClick = Number(e.target.id.slice(2)) - 1;
				//alert('true');
				this.infoMovie(idClick);
			}
			if (e.target.id === 'close') {
				//alert('it is working');
				this.closeInfo();
			}
		},
		attachEvents: function () {
			window.addEventListener('hashchange', this.updateMovies.bind(this));
			window.addEventListener('click', this.checkClick.bind(this));
		}
	}
})(axios, Mustache);
